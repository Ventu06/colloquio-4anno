.PHONY: all clean

all:
	latexmk main.tex

clean:
	latexmk -C
	rm -f main.snm main.nav main.bbl main.run.xml bibliography.bib
