\documentclass[10pt, xcolor=dvipsnames]{beamer}

\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{biblatex}
\usepackage{csquotes}
\usepackage{etoolbox}
\usepackage{setspace}
\usepackage{tikz}
\usepackage{tkz-graph}

\uselanguage{Italian}
\languagepath{Italian}

\usetheme{Madrid}

\newcommand{\zerodisplayskips}{%
	\setlength{\abovedisplayskip}{3pt}%
	\setlength{\belowdisplayskip}{3pt}%
	\setlength{\abovedisplayshortskip}{3pt}%
	\setlength{\belowdisplayshortskip}{3pt}%
}
\appto{\normalsize}{\zerodisplayskips}
\appto{\small}{\zerodisplayskips}
\appto{\footnotesize}{\zerodisplayskips}

\DeclareMathOperator\Aut{Aut}
\DeclareMathOperator\Frob{Frob}
\DeclareMathOperator\Id{Id}
\DeclareMathOperator\Sym{Sym}

\newcommand{\Nb}{\mathbb{N}}
\newcommand{\Zb}{\mathbb{Z}}
\newcommand{\Xc}{\mathcal{X}}
\newcommand{\Yc}{\mathcal{Y}}

\newenvironment<>{definitionblock}[1]{%
	\setbeamercolor{block title}{fg=white,bg=blue!75!black}%
	\begin{block}#2{#1}}{\end{block}%
}
\newenvironment<>{theoremblock}[1]{%
	\setbeamercolor{block title}{fg=white,bg=green!40!black}%
	\begin{block}#2{#1}}{\end{block}%
}
\newenvironment<>{problemblock}[1]{%
	\setbeamercolor{block title}{fg=white,bg=red!75!black}%
	\begin{block}#2{#1}}{\end{block}%
}
\renewenvironment<>{exampleblock}[1]{%
	\setbeamercolor{block title}{fg=white,bg=orange!85!Black}%
	\begin{block}#2{#1}}{\end{block}%
}

\title[Reciprocità combinatorica]{%
	Reciprocità combinatorica:%
	\texorpdfstring{\\}{}%
	relazione tra grafi e gruppi di automorfismo
}
\author[Eduardo Venturini]{%
	Candidato: Eduardo Venturini%
	\texorpdfstring{\\}{}%
	Relatore: prof. Michele D'Adderio%
}
\institute[SNS]{Scuola Normale Superiore}
\date{26 Aprile 2023}

\begin{filecontents}{bibliography.bib}

	@book{beck2018combinatorial,
		title={Combinatorial reciprocity theorems},
		author={Beck, Matthias and Sanyal, Raman},
		volume={195},
		year={2018},
		publisher={American Mathematical Soc.}
	}

	@article{Cameron2018,
		%doi = {10.37236/7299},
		%url = {https://doi.org/10.37236/7299},
		year = {2018},
		month = jan,
		publisher = {The Electronic Journal of Combinatorics},
		volume = {25},
		number = {1},
		author = {Peter J. Cameron and Jason Semeraro},
		title = {The Cycle Polynomial of a Permutation Group},
		journal = {The Electronic Journal of Combinatorics}
	}

	@article{campbell2020groupgraph,
		title={Group-Graph Reciprocal Pairs},
		author={Kirsty Campbell},
		year={2020},
		eprint={2005.13566},
		archivePrefix={arXiv},
		primaryClass={math.CO}
	}

	@article{Palmer1973,
		%doi = {10.1007/bf02392038},
		%url = {https://doi.org/10.1007/bf02392038},
		year = {1973},
		publisher = {International Press of Boston},
		volume = {131},
		number = {0},
		pages = {123--143},
		author = {E. M. Palmer and R. W. Robinson},
		title = {Enumeration under two representations of the wreath product},
		journal = {Acta Mathematica}
	}

	@article{wang2020nonschurpositivity,
		title={Non-Schur-positivity of chromatic symmetric functions},
		author={David G. L. Wang and Monica M. Y. Wang},
		year={2020},
		eprint={2001.00181},
		archivePrefix={arXiv},
		primaryClass={math.CO}
	}

	@article{loehr2019quasisymmetric,
		title={Quasisymmetric and Schur expansions of cycle index polynomials},
		author={Loehr, Nicholas A and Warrington, Gregory S},
		journal={Discrete Mathematics},
		volume={342},
		number={1},
		pages={113--127},
		year={2019},
		publisher={Elsevier}
	}

\end{filecontents}

\addbibresource{bibliography.bib}


\begin{document}


\frame{\titlepage}


\begin{frame}
	\frametitle{Introduzione}

	\pause
	\begin{definitionblock}{Reciprocità combinatorica - R. Stanley - 1974}
		\pause
		$\Xc, \Yc$ classi di oggetti combinatorici con una grandezza $g_{\Xc}: \Xc \to \Nb$
		\[ f_{\Xc}(n) = |\{ x \in \Xc \mid g(x) = n \}| \]
		\vspace*{-\baselineskip}\pause
		\[ \pm f_{\Xc}(-n) = f_{\Yc}(n) \quad \Longrightarrow \quad \text{reciprocità combinatorica!} \]
	\end{definitionblock}

	\pause
	\begin{exampleblock}{Esempio 0}
		Sia $d \in \Nb$

		\begin{center}
			\begin{tabular}{c c}
				$\Xc_n = \{ \substack{\text{sottoinsiemi di cardinalità $d$} \\ \text{di un insieme di $n$ elementi}} \}$
				&
				$\Yc_n = \{ \substack{\text{sottoinsiemi con ripetizioni di cardinalità $d$} \\ \text{di un insieme di $n$ elementi}} \}$
				\\[\medskipamount]
				$\Xc = \bigsqcup_{n \in \Nb} \Xc_n$
				&
				$\Yc = \bigsqcup_{n \in \Nb} \Yc_n$
				\\[\medskipamount]
				$g_{\Xc}(x) = n \mid x \in \Xc_n$
				&
				$g_{\Yc}(x) = n \mid x \in \Yc_n$
				\\[\medskipamount]
				$f_{\Xc}(n) = |\Xc_n| = {n \choose d}$
				&
				$f_{\Yc}(n) = |\Yc_n| = {n+d-1 \choose d}$
				\\[\bigskipamount]
				\multicolumn{2}{c}{$f_{\Xc}(-n) = \tfrac{1}{d!} (-n)_d = (-1)^d \tfrac{1}{d!} (n+d-1)_d = (-1)^d f_{\Yc}(n)$}
			\end{tabular}
		\end{center}
	\end{exampleblock}

\end{frame}


\begin{frame}
	\frametitle{Grafi e colorazioni}

	\pause
	$P =(V,E)$ grafo semplice
	\begin{minipage}{0.75\textwidth}%
		\begin{definitionblock}{Colorazione}
			$c : V \to \{1, \dots, n\}$ colorazione con $n$ colori\\
			$c$ è propria se $(i,j) \in E \Rightarrow c(i) \neq c(j)$.
		\end{definitionblock}
	\end{minipage}%
	\hfill%
	\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph0}}\end{minipage}

	\pause
	\begin{definitionblock}{Polinomio cromatico}
		\vspace*{-\medskipamount}
		\[ Z_P(n) = |\{ \text{colorazioni proprie di $P$ con $n$ colori} \}| \]
	\end{definitionblock}

	\pause
	\[ Z_P(n) = Z_{P + e}(n) + Z_{P / e}(n) \]

	\begin{center}
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph1_0}}\end{minipage}
		$\quad = \quad$
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph1_1}}\end{minipage}
		$\quad + \quad$
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph1_2}}\end{minipage}
	\end{center}

\end{frame}


\begin{frame}
	\frametitle{Reciprocità fra grafi e gruppi di automorfismo}

	\pause
	$G < S_d$ gruppo di permutazioni

	\begin{definitionblock}{Polinomio ciclico}
		\[ C_G(x) = \frac{1}{|G|} \sum_{g \in G} x^{c(g)} \]
		$c(g)$ numero di cicli di $g$ nella sua decomposizione in cicli
	\end{definitionblock}

	\pause
	\medskip
	Se $G$ agisce su $d$ elementi colorabili con $n$ colori, $C_G(n)$ conta il numero di orbite (lemma di Burnside)

\end{frame}


\begin{frame}
	\frametitle{Reciprocità fra grafi e gruppi di automorfismo}

	\pause
	$P = (V,E)$ grafo semplice.

	\begin{minipage}{0.75\textwidth}%
		\begin{definitionblock}{Gruppo di automorfismi}
			\[ \Aut(P) = \left\{ \substack{\varphi : V \to V \text{ bigezione}\\ (i,j) \in E \iff (\varphi(i), \varphi(j)) \in E} \right\} \]
		\end{definitionblock}
		Nel grafo a destra, $\Aut(P) = \Zb/2\Zb \times \Zb/2\Zb$.
	\end{minipage}%
	\hfill%
	\begin{minipage}{0.2\textwidth}%
		\resizebox{\textwidth}{!}{\input{res/graph3}}
	\end{minipage}

	\pause
	\bigskip
	$G < \Aut(P)$

	\begin{definitionblock}{Polinomio cromatico orbitale}
		\[ Z_{P,G}(x) = \frac{1}{|G|} \sum_{g \in G} Z_{P,g}(x) \]
		$Z_{P,g}(n) = |\{ \text{colorazioni proprie di $P$ con $n$ colori $g$-invarianti} \}|$
	\end{definitionblock}

	\pause
	\medskip
	Se $G$ agisce su $P$, colorando i vertici con $n$ colori, $Z_{P,G}(n)$ conta il numero di orbite (lemma di Burnside)

\end{frame}


\begin{frame}
	\frametitle{Reciprocità fra grafi e gruppi di automorfismo}

	\pause
	\begin{problemblock}{Problema - P. J. Cameron, J. Semeraro - 2017}
		$(P,G)$ con $G < \Aut(P)$ è una coppia reciproca se
		\[ (-1)^{|V|} C_G(-x) = Z_{P,G}(x) \]
		Trovare tutte le coppie reciproche!
	\end{problemblock}

	\pause
	\medskip
	Perché proprio $C_G$ e $Z_{P,G}$?
	\begin{itemize}
		\item $(P_1,G_1)$ e $(P_2,G_2)$ reciproche $\Longrightarrow$ $(P_1,G_1) \times (P_2,G_2)$ reciproca
		\item $(P,G)$ reciproca, $H < A_h$ $\Longrightarrow$ $(P,G) \wr H$ reciproca
		\item esistono classi e casi sporadici di coppie reciproche
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Reciprocità fra grafi e gruppi di automorfismo}

	\pause
	\begin{theoremblock}{Prodotto diretto}
		Date $(P_1, G_1)$ e $(P_2, G_2)$ costruiamo $(P_1,G_1) \times (P_2,G_2) = (P_1 \sqcup P_2, G_1 \times G_2)$
		\pause
		\begin{align*}
			C_{G_1 \times G_2}(x) &= C_{G_1}(x) \cdot C_{G_2}(x)\\
			Z_{(P_1, G_1) \times (P_2, G_2)}(x) &= Z_{P_1, G_1}(x) \cdot Z_{P_2, G_2}(x)
		\end{align*}
		\pause
		Se $(P_1,G_1)$ e $(P_2,G_2)$ sono reciproche, anche $(P_1,G_1) \times (P_2,G_2)$ è reciproca
	\end{theoremblock}

	\pause
	\begin{theoremblock}{Prodotto intrecciato}
		Dati $(P,G)$ e $H < S_h$, costruiamo $(P,G) \wr H = (\bigsqcup_{i=1}^h P, (\bigoplus_{i=1}^h G) \rtimes H)$
		\pause
		\begin{align*}
			C_{G \wr H}(x) &= C_H(C_G(x))\\
			Z_{(P,G) \wr H}(x) &= C_H(Z_{P,G}(x))
		\end{align*}
		\pause
		Se $(P,G)$ è reciproca e $H < A_h$, anche $(P,G) \wr H$ è reciproca
	\end{theoremblock}

\end{frame}


\begin{frame}
	\frametitle{Risultati e congetture}

	\pause
	\begin{exampleblock}{Grafi completi $K_d$}
		\begin{minipage}{0.75\textwidth}
			$K_d$ grafo completo con $d$ vertici, $G < S_d$
			\[ Z_{K_d,G}(x) = \tfrac{1}{|G|} Z_{K_d}(x) = \tfrac{1}{|G|} x(x-1) \dots (x-(d-1)) = \tfrac{1}{|G|} (x)_d \]
			invece solo per $G = S_d$
			\[ C_G(x) = \tfrac{1}{|G|} x(x+1) \dots (x+(d-1)) = \tfrac{1}{|G|} (x)^d = \tfrac{1}{|G|}(-1)^d (-x)_d \]
			quindi $(K_d,S_d)$ è reciproca
		\end{minipage}%
		\hfill%
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph4}}\end{minipage}
	\end{exampleblock}

	\pause
	\begin{exampleblock}{Grafo ciclico $Q_4$}
		\begin{minipage}{0.75\textwidth}
			$Q_4$ il grafo ciclico con $4$ vertici, $D_4 = \Aut(Q_4)$ gruppo diedrale
			\begin{align*}
				Z_{Q_4,D_4}(x) &= \tfrac{1}{8} x(x-1)(x^2-x+2)\\
				C_{D_4}(x) &= \tfrac{1}{8} x(x+1)(x^2+x+2)
			\end{align*}
			quindi $(Q_4,D_4)$ è reciproca
		\end{minipage}%
		\hfill%
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph5}}\end{minipage}
	\end{exampleblock}

\end{frame}


\begin{frame}
	\frametitle{Risultati e congetture}

	\pause
	$(P,G)$ reciproca, con $P$ grafo ad albero

	\pause
	\medskip
	Grazie a:
	\begin{itemize}
		\item $Z_{P}(x) = x(x-1)^{|V|-1}$
		\item $P/g$ è un albero ($g \in G$)
		\item $\dots$
	\end{itemize}

	\pause
	\begin{exampleblock}{$1$-stelle}
		\begin{minipage}{0.75\textwidth}
			$P$ è una $1$-stella, costituita da un vertice ``centrale'' collegato ad un numero pari di vertici ``esterni'';
			inoltre, posto $|V| = 2k+1$, si ha $(S_2)^k < G < (S_2)^k \wr S_k$
		\end{minipage}%
		\hfill%
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph6}}\end{minipage}
	\end{exampleblock}

\end{frame}


\begin{frame}
	\frametitle{Risultati e congetture}

	\pause
	Esiste una classe di coppie reciproche che generalizza sia le $1$-stelle che i grafi completi: le stelle!

	\pause
	\begin{exampleblock}{Stelle}
		\begin{minipage}{0.75\textwidth}
			$T_{k,r}$ grafo composto da un grafo completo di $k \ge 1$ vertici ``centrali'',
			tutti collegati a $r(k+1)$ vertici ``esterni'' senza archi fra loro	($r \ge 0$)

			$G = \bar{G} \times S_k$ con
			\begin{itemize}
				\item $(S_{k+1})^r < \bar{G} < (S_{k+1})^r \wr S_r$ per $k$ dispari;
				\item $(S_{k+1})^r < \bar{G} < (S_{k+1})^r \wr A_r$ per $k$ pari;
			\end{itemize}

			$(T_{k,r},G)$ è una coppia reciproca
		\end{minipage}%
		\hfill%
		\begin{minipage}%
			{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph7}}%
			\begin{center}$T_{2,2}$\end{center}%
		\end{minipage}
	\end{exampleblock}


\end{frame}


\begin{frame}
	\frametitle{Risultati e congetture}

	\pause
	$(P,G)$ reciproca è irriducibile se non si ottiene per prodotto diretto o intrecciato da altre coppie reciproche.

	\pause
	\medskip
	\begin{problemblock}{Congettura - K. Campbell, J. Semeraro - 2020}
		Le coppie reciproche irriducibili sono
		\begin{itemize}
			\item $(Q_4, D_4)$, cioè il $4$-ciclo con il gruppo diedrale;
			\item $T_{k,r}$, al variare di $k \ge 1$, $r \ge 0$ e del gruppo $G$ come descritto in precedenza;
		\end{itemize}
	\end{problemblock}

	\pause
	\medskip
	Verifica computazionale per $|V|, |E| \le 20$.

	\pause
	Complessità esponenziale dell'algoritmo:
	\begin{itemize}
		\item trovare $\Aut(P)$ richiede tempo non polinomiale
		\item calcolare $Z_P(x)$ è NP-hard
		\item calcolare e iterare sui sottogruppi di $\Aut(P)$ è dispendioso
	\end{itemize}

	\pause
	Possibili miglioramenti:
	\begin{itemize}
		\item restringersi a classi più specifiche
		\item tecniche di pruning: comparare coefficienti di $C_G(x)$ e $Z_{P,G}(x)$ facilmente calcolabili, \dots
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Introduzione alle funzioni simmetriche}

	\pause
	\begin{definitionblock}{Partizioni}
		$n \in \Nb$, $\lambda = (\lambda_1, \dots, \lambda_j)$ partizione di $n$ se
		\[ \lambda_1 + \dots + \lambda_j = n \quad , \quad \lambda_1 \ge \lambda_2 \ge \dots \ge \lambda_j > 0 \]
	\end{definitionblock}

	\pause
	\medskip
	Notazione:
	\begin{itemize}
		\item $|\lambda|$: grandezza di $\lambda$ (cioè $n$)
		\item $l(\lambda)$: lunghezza di $\lambda$ (cioè $j$)
		\item $(n^j)$: $(\underbrace{n,n, \dots, n}_{j \text{ volte}})$
			\pause
		\item data $\alpha = (\alpha_1, \alpha_2, \dots, \alpha_j)$ sequenza in $\Nb$, $x^{\alpha}: x_1^{\alpha_1} x_2^{\alpha_2} \dots x_j^{\alpha_j}$
		\item $\alpha \sim \lambda$: togliendo i valori nulli, $\alpha$ è una permutazione di $\lambda$
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Introduzione alle funzioni simmetriche}

	\pause
	\begin{definitionblock}{Funzioni simmetriche}
		$k$ campo di caratteristica $0$, $(x_j)_{j \in \Nb^+}$ variabili.

		Le funzioni simmetriche sono le serie formali in $k[(x_j)_{j \in \Nb}] = k[X]$ di grado limitato, invarianti per permutazione delle variabili.

		Le funzioni simmetriche costituiscono l'algebra $\Sym[X]$.
	\end{definitionblock}

	\pause
	\medskip
	Alcune famiglie di funzioni simmetriche:
	\begin{itemize}
		\pause
		\item le funzioni simmetriche monomiali:
			\[ m_{\lambda} = \sum_{\alpha \sim \lambda} x^{\alpha} \]
			Es: $m_{(2,1,1)} = x_1^2 x_2 x_3 + x_1 x_2^2 x_3 + x_1 x_2 x_3^2 + x_1^2 x_2 x_4 + \dots$
			\pause
		\item le funzioni simmetriche somme di potenze:
			\[
				p_{\lambda} = p_{\lambda_1} p_{\lambda_2} \dots p_{\lambda_{l(\lambda)}}
				\quad \text{con} \quad
				p_j = \sum_{\alpha \sim (j)} x^{\alpha} = \sum_{i} x_i^j
			\]
			Es: $p_{(2,1,1)} = (x_1^2 + x_2^2 + x_3^2 + \dots) (x_1 + x_2 + x_3 + \dots)^2$
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Introduzione alle funzioni simmetriche}

	\pause
	\begin{theoremblock}{Basi delle funzioni simmetriche}
		$(m_{\lambda})$ e $(p_{\lambda})$, al variare di $\lambda$ fra le partizioni dei numeri naturali, sono basi di $\Sym[X]$ come spazio vettoriale

		$(p_j)_{j \in \Nb}$ sono algebricamente indipendenti
	\end{theoremblock}

	\pause
	\bigskip
	\begin{definitionblock}{Involuzione $\omega$}
		Sia $\omega : \Sym[X] \to \Sym[X]$ dato da
			\[ \omega(p_{\lambda}) = (-1)^{|\lambda| - l(\lambda)} p_{\lambda} \]
		$\omega$ è un automorfismo e un'involuzione, cioè $\omega^2 = \Id$
	\end{definitionblock}

\end{frame}


\begin{frame}
	\frametitle{Riformulazione con funzioni simmetriche}

	\pause
	$G < S_d$ gruppo di permutazioni

	\begin{definitionblock}{Funzione simmetrica ciclica}
		\[ C_G = \frac{1}{|G|} \sum_{g \in G} p_{\lambda(g)} = \frac{1}{|G|} \sum_{g \in G} p_1^{c_1(g)} p_2^{c_2(g)} \dots \]
		dove $c_j(g)$ conta il numero di cicli di $g$ contenenti $j$ elementi e $\lambda(g) \sim (j^{c_j(g)})_{j \in \Nb}$
	\end{definitionblock}

	\pause
	\medskip
	$(p_j)_{j \in \Nb}$ sono algebricamente indipendenti e generano $\Sym[X]$,
	sia $\psi : \Sym[X] \to k[x]$ dato da $\psi(p_j) = x$ per ogni $j$.

	\medskip
	$\psi(p_{\lambda(g)}) = x^{\sum_{j} c_j(g)} = x^{c(g)}$, quindi $\psi(C_G) = C_G(x)$

\end{frame}


\begin{frame}
	\frametitle{Riformulazione con funzioni simmetriche}

	\pause
	$P$ grafo semplice

	\begin{minipage}{0.75\textwidth}%
		\begin{definitionblock}{Funzione simmetrica cromatica}
			Data $c$ colorazione propria di $P$ con $\Nb$ colori, consideriamo
			\[ x^{|c^{-1}|} = x_1^{|c^{-1}(1)|} x_2^{|c^{-1}(2)|} \dots\]
			Sia
			\[ Z_P = \sum_{c \ \substack{\text{colorazione}\\ \text{propria di $P$}}} x^{|c^{-1}|} \]
		\end{definitionblock}
	\end{minipage}%
	\hfill%
	\begin{minipage}%
		{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph0}}\\
		\begin{center}Monomio: $x_1 x_2^3 x_3$\end{center}
	\end{minipage}

	\pause
	\medskip
	Specializzando $x_1, x_2, \dots, x_n \mapsto 1$ e $x_{n+1}, x_{n+2}, \dots \mapsto 0$, $Z_P \mapsto Z_P(n)$.

	\medskip
	Ma così $p_j \mapsto n$ per ogni $j$, quindi $\psi(Z_{P}) = Z_{P}(x)$.

\end{frame}


\begin{frame}
	\frametitle{Riformulazione con funzioni simmetriche}

	\pause
	$P$ grafo semplice, $G < \Aut(P)$.
	\begin{definitionblock}{Funzione simmetrica cromatica orbitale}
		\[ Z_{P,G} = \frac{1}{|G|} \sum_{g \in G} Z_{P,g} \]
		con $Z_{P,g}$ funzione simmetrica cromatica che considera solo le colorazioni di $P$ $g$-invarianti.
	\end{definitionblock}

	\medskip
	Anche qui $\psi(Z_{P,G}) = Z_{P,G}(x)$.

\end{frame}


\begin{frame}
	\frametitle{Riformulazione con funzioni simmetriche}

	\pause
	\begin{problemblock}{Problema}
		$(P,G)$ con $G < \Aut(P)$ è una coppia reciproca se
		\[ \omega(C_G) = Z_{P,G} \]
		Trovare tutte le coppie reciproche!
	\end{problemblock}

	\pause
	\medskip
	$\psi(\omega(f))(x) = (-1)^d f(-x)$, quindi
	\[ \omega(C_G) = Z_{P,G} \quad \Longrightarrow \quad (-1)^{|V|} C_G(-x) = Z_{P,G}(x) \]

	Per trovare queste nuove coppie reciproche è sufficiente controllare le coppie reciproche nel caso polinomiale.
	Se la congettura è vera, alla fine della presentazione avremo classificato tutte le nuove coppie reciproche.

	\pause
	\medskip
	Valgono ancora i motivi che ci hanno spinto a scegliere $C_G$ e $Z_{P,G}$?
	\begin{itemize}
		\item $(P_1,G_1)$ e $(P_2,G_2)$ reciproche $\Longrightarrow$ $(P_1,G_1) \times (P_2,G_2)$ reciproca
		\item $(P,G)$ reciproca, $H < A_h$ $\Longrightarrow$ $(P,G) \wr H$ reciproca
		\item esistono classi e casi sporadici di coppie reciproche
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Riformulazione con funzioni simmetriche}

	\pause
	\begin{theoremblock}{Prodotto diretto}
		Il prodotto diretto funziona!

		\pause
		\medskip
		Date $(P_1, G_1)$ e $(P_2, G_2)$
		\begin{align*}
			C_{G_1 \times G_2} &= C_{G_1} \cdot C_{G_2}\\
			Z_{(P_1, G_1) \times (P_2, G_2)} &= Z_{P_1, G_1} \cdot Z_{P_2, G_2}
		\end{align*}

		\medskip
		Se $(P_1,G_1)$ e $(P_2,G_2)$ sono reciproche, anche $(P_1,G_1) \times (P_2,G_2)$ è reciproca
	\end{theoremblock}

	\pause
	\begin{theoremblock}{Prodotto intrecciato}
		Il prodotto intrecciato funziona!

		\pause
		\medskip
		Dati $(P,G)$ e $H < S_h$
		\begin{align*}
			C_{G \wr H}(p_1, p_2, \dots) &= C_H(C_G(p_1, p_2, \dots), C_G(p_2, p_4, \dots), C_G(p_3, p_6, \dots), \dots)\\
			Z_{(P,G) \wr H}(p_1, p_2, \dots) &= C_H(Z_{P,G}(p_1, p_2, \dots), Z_{P,G}(p_2, p_4, \dots), Z_{P,G}(p_3, p_6, \dots), \dots)
		\end{align*}
		(tutte le funzioni sono nella base $(p_j)_{j \in \Nb}$)

		\medskip
		Se $(P,G)$ è reciproca e $H < A_h$, anche $(P,G) \wr H$ è reciproca
	\end{theoremblock}


\end{frame}


\begin{frame}
	\frametitle{Riformulazione con funzioni simmetriche}

	\pause
	\begin{theoremblock}{Grafi base}
		I grafi completi e il 4-ciclo funzionano!
	\end{theoremblock}

	\pause
	\begin{exampleblock}{Grafi completi $K_d$}
		\begin{minipage}{0.75\textwidth}
			$K_d$ grafo completo con $d$ vertici
			\begin{align*}
				Z_{K_d, S_d} &= \textstyle\sum_{i_1 < i_2 < \dots < i_d} x_{i_1} x_{i_2} \dots x_{i_d}\\
							 &= \omega(\textstyle\sum_{i_1 \le i_2 \le \dots \le i_d} x_{i_1} x_{i_2} \dots x_{i_d})
							 = \omega(C_{S_d})
			\end{align*}
			quindi $(K_d,S_d)$ è reciproca.
		\end{minipage}%
		\hfill%
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph4}}\end{minipage}
	\end{exampleblock}

	\pause
	\begin{exampleblock}{Grafo ciclico $Q_4$}
		\begin{minipage}{0.75\textwidth}
			$Q_4$ grafo ciclico con $4$ vertici
			\begin{align*}
				Z_{Q_4,D_4} &= \tfrac{1}{8} (-2p_{(4)} + 3p_{(2,2)} - 2p_{(2,1,1)} + p_{(1,1,1,1)})\\
							&= \omega(\tfrac{1}{8} (2p_{(4)} + 3p_{(2,2)} + 2p_{(2,1,1)} + p_{(1,1,1,1)})) = \omega(C_{D_4})
			\end{align*}
			quindi $(Q_4,D_4)$ è reciproca.
		\end{minipage}%
		\hfill%
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph5}}\end{minipage}
	\end{exampleblock}

\end{frame}


\begin{frame}
	\frametitle{Riformulazione con funzioni simmetriche}

	\pause
	\begin{problemblock}{Stelle}
		Non tutte le stelle funzionano.
	\end{problemblock}

	\pause
	\begin{exampleblock}{Stella $T_{1,2}$}
		\begin{minipage}{0.75\textwidth}
			$T_{1,2}$ stella e $G \simeq S_2 \times S_2$ che agisce sulle $4$ punte
			\begin{align*}
				C_G(x) &= x^5 + 2x^4 + x^3\\
				Z_{T_{1,2},G}(x) &= x^5 - 2x^4 + x^3
			\end{align*}

			Invece
			\begin{align*}
				C_G &= \tfrac{1}{4} (p_{(2,2,1)} + 2p_{(2,1,1,1)} + p_{(1,1,1,1,1)})\\
				Z_{T_{1,2},G} &= \tfrac{1}{4} (4p_{(3,1,1)} - 3p_{(2,2,1)} - 2p_{(2,1,1,1)} + p_{(1,1,1,1,1)})
			\end{align*}

			quindi
			\[ \omega(C_G) \neq Z_{T_{1,2},G} \]
		\end{minipage}%
		\hfill%
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph8}}\end{minipage}
	\end{exampleblock}

\end{frame}


\begin{frame}
	\frametitle{Il caso delle stelle}

	\pause
	$P$ è $\lambda$-colorabile se esiste $c$ colorazione propria con $|c^{-1}(j)| = \lambda_j$,
	cioè se $m_{\lambda}$ compare in $Z_P$ scritta nella base monomiale.

	\pause
	\medskip
	$\lambda \succeq \mu$ se:
	\begin{itemize}
		\item $|\lambda| = |\mu|$
		\item $\sum_{j=1}^k \lambda_j \ge \sum_{j=1}^k \mu_j \quad \forall \ 1 \le k \le l(\lambda)$
	\end{itemize}

	\pause
	\medskip
	\begin{theoremblock}{Criterio delle colorazioni dominanti}
		$(P,G)$ reciproca. Se $P$ è $\lambda$-colorabile, allora è anche $\mu$-colorabile per ogni $\mu \preceq \lambda$
	\end{theoremblock}

	\pause
	Osservazioni:
	\begin{itemize}
		\pause
		\item $G$ non compare nel criterio
			\pause
		\item la dimostrazione usa fatti di teoria delle rappresentazioni di $S_n$ per cogliere l'essenza di gruppo di $G$
			\pause
		\item ci si riconduce a delle condizioni che legano le rappresentazioni di $S_n$ e le colorazioni di $P$
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Il caso delle stelle}

	\pause
	\begin{theoremblock}{Classificazione delle stelle reciproche}
		Le uniche stelle reciproche sono $T_{k,0}$ per ogni $k \ge 1$ e $T_{1,1}$
	\end{theoremblock}

	\pause
	\begin{problemblock}{Dimostrazione}
		\begin{itemize}
			\pause
			\item $T_{k,0} = K_k$ ammettono reciprocità, sia $(T_{k,r},G)$ reciproca con $r \ge 1$
				\pause
			\item possiamo colorare il centro della stella con $k$ colori diversi e le punte con un altro colore,
				quindi $T_{k,r}$ è $\lambda = (r(k+1), 1^k)$-colorabile
				\pause
			\item se $(k,r) \neq (1,1)$, si ha $r(k+1)>2$, quindi
				\[ \lambda = (r(k+1), 1^k) \succ (r(k+1)-1, 2, 1^{k-1}) = \mu \]
				per il criterio $P$ è $\mu$-colorabile
				\pause
			\item ognuno dei $k$ vertici centrali ha un colore unico, ma se $(r,k) \neq (1,1)$, $\mu$ ha solo $k-1$ colori unici, assurdo
				\pause
			\item rimane $T_{1,1}$, con $G \simeq S_2$ che agisce sulle $2$ punte
				\[ \omega(C_G) = \omega(\tfrac{1}{2} (p_{(2,1)} + p_{(1,1,1)})) = \tfrac{1}{2} (- p_{(2,1)} + p_{(1,1,1)}) = Z_{T_{1,1},G} \]
				quindi $(T_{1,1},G)$ è reciproca
		\end{itemize}
	\end{problemblock}

\end{frame}


\begin{frame}
	\frametitle{Bibliografia}

	\nocite{*}
	\printbibliography[heading=bibintoc]

\end{frame}


\end{document}
